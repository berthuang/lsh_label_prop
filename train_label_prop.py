from lsh_label_prop import *
import numpy as np
import pandas as pd
import codecs, datetime, glob, itertools, os
import re, sklearn, string, sys, tensorflow, time
import tensorflow as tf
from tensorflow.python import keras
from keras.utils.vis_utils import model_to_dot
from keras import backend as K, regularizers, optimizers
from keras.models import load_model, Sequential
from keras.layers import MaxPooling2D, Conv2D, Activation, Dropout, Flatten, Dense, InputLayer
from keras.layers.normalization import BatchNormalization
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils import np_utils
from keras import optimizers
from keras.regularizers import L1L2
from sklearn.metrics import classification_report, confusion_matrix, f1_score
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score
import codecs, json, random
from load_data import *
from sklearn.model_selection import StratifiedKFold


def create_model(img_rows, img_cols, channels):

    model = Sequential()
    model.add(
        Conv2D(32,
               kernel_size=3,
               input_shape=(img_rows, img_cols, channels),
               padding="same"))

    model.add(Activation('relu'))
    model.add(Conv2D(32, 3, padding="same"))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(Dropout(0.3))

    model.add(Conv2D(64, 3))
    model.add(Activation('relu'))
    model.add(Conv2D(64, 3, padding="same"))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(Dropout(0.3))

    model.add(Conv2D(128, 3))
    model.add(Activation('relu'))
    model.add(Conv2D(128, 3, padding="same"))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=2))
    model.add(Dropout(0.3))

    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.3))

    model.add(Dense(10, activation='softmax'))

    # lr = learning_rate
    # optimizers = {"SGD": keras.optimizers.SGD(lr=lr), "RMSprop": keras.optimizers.RMSprop(lr=lr),

    #               "Adadelta": keras.optimizers.Adadelta(lr=lr), "Adam": keras.optimizers.Adam(lr=lr)}

    # model.compile(loss=keras.losses.categorical_crossentropy,
    #               optimizer=optimizers[optimizer], metrics=['accuracy'])

    model.compile(loss='categorical_crossentropy',
                  optimizer='sgd',
                  metrics=['accuracy'])

    return model


def run_lsh_prop(data_set, labeled_set=0.01):

    results = {}
    indexes = []

    train_data, train_labels = data_set['train_data']
    test_data, test_labels = data_set['test_data']

    # normalize data
    normalize = 1 / 255
    train_data = train_data * normalize
    test_data = test_data * normalize

    num_classes = data_set['num_classes']
    img_rows, img_cols = data_set['img_rows'], data_set['img_cols']
    channels = data_set['channels']

    num_data_points = int(labeled_set * train_labels.size / num_classes)

    # Get datapoints to evaluate bounds on
    for label in range(num_classes):
        index = get_datapoints(train_labels, num_data_points, label)
        indexes = np.append(indexes, index)

    indexes = indexes.astype(int)
    labeled_data, labeled_labels = train_data[indexes], train_labels[indexes]

    # delete the indexes
    train_data = np.delete(train_data, indexes, axis=0)
    train_labels = np.delete(train_labels, indexes)

    ##### block for k-fold validation #####
    X = labeled_data.copy()
    y = labeled_labels.copy()
    num_splits = 4
    error_rates = []
    precisions = []

    # create k-fold cross validation
    skf = StratifiedKFold(n_splits=num_splits)
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]

        num_test = y_test.size
        X_concat = np.vstack((train_data, X_test))

        print("Running k fold...")
        # run lsh label prop for each fold
        start_time = time.time()
        predicted_labels = lsh_label_prop(X_train.T, y_train, X_concat.T)
        cost = time.time() - start_time

        print("num_predicted_labels", np.unique(predicted_labels))

        k = 1
        print("Fold %d, LSH label spreading took in %f seconds." % (k, cost))

        predicted_labels = keras.utils.to_categorical(predicted_labels,
                                                      num_classes)
        y_test = keras.utils.to_categorical(y_test, num_classes)

        # calculate error for each class
        assert y_test.shape[0] == predicted_labels[-num_test:, :].shape[0]
        score = np.sum(y_test * (1 - predicted_labels[-num_test:, :]) +
                       predicted_labels[-num_test:, :] * (1 - y_test),
                       axis=0) / num_test
        error_rates.append(score.tolist())

        # calculate precision for each class
        score = np.sum(y_test * predicted_labels[-num_test:, :],
                       axis=0) / np.sum(predicted_labels[-num_test:, :],
                                        axis=0)
        precisions.append(score.tolist())

        k += 1
        ##### block for k-fold validation #####

    # run lsh label prop and get labels for unlabeled data
    start_time = time.time()
    predicted_labels = lsh_label_prop(labeled_data.T, labeled_labels,
                                      train_data.T)
    cost = time.time() - start_time
    # calculate accuracy of predicted labels
    predicted_accuracy = accuracy_score(train_labels, predicted_labels)
    lsh_prop_label_error = 1 - predicted_accuracy
    results['lsh_label_error'] = lsh_prop_label_error

    print("LSH label propagation scored %f accuracy in %f seconds." %
          (predicted_accuracy, cost))

    train_data = np.vstack((train_data, labeled_data))
    train_data = train_data.reshape(train_data.shape[0], img_rows, img_cols,
                                    channels)
    lsh_train_labels = np.concatenate((predicted_labels, labeled_labels))
    lsh_train_labels = keras.utils.to_categorical(lsh_train_labels,
                                                  num_classes)

    train_labels = np.concatenate((train_labels, labeled_labels))
    train_labels = keras.utils.to_categorical(train_labels, num_classes)
    test_labels = keras.utils.to_categorical(test_labels, num_classes)
    test_data = test_data.reshape(test_data.shape[0], img_rows, img_cols,
                                  channels)

    model = create_model(img_rows, img_cols, channels)
    model.fit(train_data,
              lsh_train_labels,
              batch_size=32,
              epochs=50,
              verbose=1)

    # calculate train results
    score = model.evaluate(train_data, train_labels, verbose=0)
    train_loss, train_accuracy = score

    # calculate test results
    score = model.evaluate(test_data, test_labels, verbose=0)
    test_loss, test_accuracy = score

    # update all results
    results['lsh_train_accuracy'] = train_accuracy
    results['lsh_test_accuracy'] = test_accuracy
    results['predicted_labels'] = predicted_labels
    results['precision'] = precisions
    results['error_rates'] = error_rates

    print("")
    print(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    )
    print("Lsh prop accuracy of learned model on the training data is",
          train_accuracy)
    print("Lsh prop of model on the test data is", test_accuracy)
    print("")
    print(
        "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    )

    # Write snorkel matrix to a file
    file_path = '../results/lsh_prop_results.json'
    json.dump(results,
              codecs.open(file_path, 'w', encoding='utf-8'),
              separators=(',', ':'),
              sort_keys=True,
              indent=4)


run_lsh_prop(load_fashion_mnist())
