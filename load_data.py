import os
import gzip
import numpy as np
from sklearn.utils import resample
import keras
from keras.datasets import cifar10


def load_cifar_10():
    # Returns dictionary of training and test data

    data = {}
    categories = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog",
              "horse", "ship", "truck"]

    # The data, split between train and test sets:
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()

    train_size, img_rows, img_cols, channels = x_train.shape
    test_size, img_rows, img_cols, channels = x_test.shape
    y_train, y_test = y_train.ravel(), y_test.ravel()

    data['img_rows'] = img_rows
    data['img_cols'] = img_cols
    data['channels'] = channels
    data['num_classes'] = 10
    data['categories'] = categories
    data['path'] = 'cifar10'

    x_train = x_train.reshape(train_size, img_rows*img_cols*channels)
    x_test = x_test.reshape(test_size, img_rows*img_cols*channels)

    data['train_data'] = x_train, y_train
    data['test_data'] = x_test, y_test

    return data


def load_fashion_mnist():
    # Returns dictionary of training and test data
    data = {}

    categories = ["t-shirt", "trouser", "pullover", "dress", "coat", "sandal", "shirt",
              "sneaker", "bag", "ankle boot"]

    def load_data(path, kind='train'):
        """Load MNIST data from `path`"""

        # label_dict = {0: 'T-shirt/top', 1: 'Trouser', 2: 'Pullover', 3: 'Dress', 4: 'Coat',
        #               5: 'Sandal', 6: 'Shirt', 7: 'Sneaker', 8: 'Bag', 9: 'Ankle boot'}

        labels_path = os.path.join(path, '%s-labels-idx1-ubyte.gz' % kind)
        images_path = os.path.join(path, '%s-images-idx3-ubyte.gz' % kind)

        with gzip.open(labels_path, 'rb') as lbpath:
            labels = np.frombuffer(lbpath.read(), dtype=np.uint8, offset=8)

        with gzip.open(images_path, 'rb') as imgpath:
            images = np.frombuffer(imgpath.read(), dtype=np.uint8,
                                   offset=16).reshape(len(labels), 784)

        return images, labels

    data['train_data'] = load_data('../../datasets/fashion-mnist', kind='train')
    data['test_data'] = load_data('../../datasets/fashion-mnist', kind='t10k')
    data['img_rows'] = 28
    data['img_cols'] = 28
    data['channels'] = 1
    data['num_classes'] = 10
    data['categories'] = categories
    data['path'] = 'fmnist'

    return data


def get_datapoints(labels, num_data_points, label_class):
    """
    Return indices for number of datapoints with specified class

    :param labels: True labels for the data
    :type labels: ndarray
    :param num_data_points: The number of datapoints to return
    :type num_data_points: int
    :param label_class: The class of the labels needed
    :type label_class: int
    :return: indices of the datapoints with specified class
    :rtype: ndarray
    """

    indices = np.where(labels == label_class)[0]
    return indices[: num_data_points]


def to_binary(labels, label_class):
    """
    Return indices for number of datapoints with specified class

    :param labels: True labels for the data
    :type labels: ndarray
    :param label_class: The class of the labels needed
    :type label_class: int
    :return: binary labels with the label class as ones
    :rtype: ndarray
    """
    binary_labels = np.zeros(labels.shape)
    binary_labels[labels == label_class] = 1
    return binary_labels


def downsample_classes(labels, label_class):
    """
    Return indices for balanced classes

    :param labels: True labels for the data
    :type labels: ndarray
    :return: indices for major and minor classes
    :rtype: ndarray
    """
    # Separate majority and minority classes
    unique_labels = np.unique(labels)
    num_class = unique_labels.size
    labels_size = labels.size

    downsample_size = int(labels_size / (num_class * (num_class - 1)))
    balanced_indices = []

    # down_sample the other class
    for label in unique_labels:
        if label != label_class:
            sample = get_datapoints(labels, downsample_size, label)
            balanced_indices.append(sample)

    balanced_indices = np.asarray(balanced_indices).ravel()
    # add the class to the samples
    sample = get_datapoints(labels, int(labels_size / num_class), label_class)
    balanced_indices = np.concatenate([balanced_indices, sample])
    # balanced_indices = balanced_indices.astype(int)
    np.random.shuffle(balanced_indices)
    # Display indices for balanced classes
    return balanced_indices



