"""Methods to do label propagation with LSH-based k-nearest neighbors"""
from sklearn.semi_supervised import LabelSpreading, LabelPropagation
from scipy.sparse import csc_matrix, coo_matrix
import numpy as np


def lsh(data_a, data_b, num_bits=10, num_hashes=10, num_neighbors=7):
	num_a, num_features = data_a.shape
	num_b = data_b.shape[0]

	hash_a = [None] * num_hashes
	hash_b = [None] * num_hashes
	weights = [None] * num_hashes
	bins = [None] * num_hashes

	for h in range(num_hashes):
		# create the ith hash function and bins
		weights[h] = np.random.randn(num_bits, num_features)

		hash_a[h] = weights[h].dot(data_a.T) > 0
		hash_b[h] = weights[h].dot(data_b.T) > 0

		bins[h] = dict()

		# not sure how to avoid these loops
		for i in range(num_b):
			key = tuple(hash_b[h][:, i])
			if key not in bins[h]:
				bins[h][key] = []
			bins[h][key] += [i]

	rows = []
	cols = []

	for i in range(num_a):
		candidates = set()
		for h in range(num_hashes):
			key = tuple(hash_a[h][:, i])

			if key in bins[h]:
				candidates.update(bins[h][key])

		candidates = np.array(list(candidates))

		distances = np.sum((data_a[i, :] - data_b[candidates, :]) ** 2, 1)

		indices = np.argsort(distances)

		num_neighbors_i = min(num_neighbors, len(candidates))

		neighbors = candidates[indices[:num_neighbors_i]]

		rows += [i] * len(neighbors)
		cols += neighbors.tolist()

	return csc_matrix(coo_matrix((np.ones(len(rows)), (rows, cols)), shape=(num_a, num_b)))


def lsh_label_prop(labeled_data, labels, unlabeled_data, kernel=lsh, propagation=False):
	model = None
	if propagation:
		model = LabelPropagation(kernel=kernel)
	else:
		model = LabelSpreading(kernel=kernel)

	num_unlabeled = unlabeled_data.shape[1]

	combined_data = np.hstack((labeled_data, unlabeled_data))
	combined_labels = np.concatenate((labels, -np.ones(num_unlabeled)))

	model.fit(combined_data.T, combined_labels)

	predicted_labels = model.transduction_

	# return only the predicted labels for the unlabeled data
	return predicted_labels[-num_unlabeled:]

