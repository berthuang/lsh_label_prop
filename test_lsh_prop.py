"""Test functions for LSH label propagation"""
import unittest
import numpy as np
import time
from lsh_label_prop import *
from sklearn.datasets import load_digits

class LshTestCase(unittest.TestCase):
    @staticmethod
    def set_up_data(num_unlabeled, num_labeled, num_features, num_classes):
        """
		Sets up a dataset with some labeled examples
		:param num_unlabeled:
		:param num_labeled:
		:param num_features:
		:param num_classes:
		:return: tuple containing the labeled examples, labels, unlabeled examples, labels
		"""

        labeled_data = np.random.randn(num_features, num_labeled)
        unlabeled_data = np.random.randn(num_features, num_unlabeled)

        weights = np.random.randn(num_features, num_classes)

        labeled_labels = labeled_data.T.dot(weights).argmax(axis=1)
        unlabeled_labels = unlabeled_data.T.dot(weights).argmax(axis=1)

        return labeled_data, labeled_labels, unlabeled_data, unlabeled_labels

    def test_small(self):
        labeled_data, labeled_labels, unlabeled_data, unlabeled_labels = self.set_up_data(
            100, 50, 5, 10)

        start_time = time.time()
        predicted_labels = lsh_label_prop(labeled_data, labeled_labels,
                                          unlabeled_data)
        cost = time.time() - start_time

        accuracy = np.mean(predicted_labels == unlabeled_labels)

        print("LSH label propagation scored %f accuracy in %f seconds." %
              (accuracy, cost))

    def test_large(self):
        labeled_data, labeled_labels, unlabeled_data, unlabeled_labels = self.set_up_data(
            1000, 500, 200, 10)

        start_time = time.time()
        predicted_labels = lsh_label_prop(labeled_data, labeled_labels,
                                          unlabeled_data)
        cost = time.time() - start_time

        accuracy = np.mean(predicted_labels == unlabeled_labels)

        print("LSH label propagation scored %f accuracy in %f seconds." %
              (accuracy, cost))

    def test_huge(self):
        labeled_data, labeled_labels, unlabeled_data, unlabeled_labels = self.set_up_data(
            20000, 600, 200, 10)



        start_time = time.time()
        predicted_labels = lsh_label_prop(labeled_data, labeled_labels,
                                          unlabeled_data)
        cost = time.time() - start_time

        accuracy = np.mean(predicted_labels == unlabeled_labels)

        print("LSH label propagation scored %f accuracy in %f seconds." %
              (accuracy, cost))

    def test_compare(self):
        # compare against using rbf kernel
        labeled_data, labeled_labels, unlabeled_data, unlabeled_labels = self.set_up_data(
            10000, 500, 200, 10)

        start_time = time.time()
        predicted_labels = lsh_label_prop(labeled_data, labeled_labels,
                                          unlabeled_data)
        cost = time.time() - start_time

        accuracy = np.mean(predicted_labels == unlabeled_labels)

        print("LSH label propagation scored %f accuracy in %f seconds." %
              (accuracy, cost))

        start_time = time.time()
        predicted_labels = lsh_label_prop(labeled_data, labeled_labels,
                                          unlabeled_data, 'rbf')
        cost = time.time() - start_time

        accuracy = np.mean(predicted_labels == unlabeled_labels)

        print("RBF label propagation scored %f accuracy in %f seconds." %
              (accuracy, cost))

    @staticmethod
    def test_digits():
        data, labels = load_digits(10, True)

        shuffled_indices = np.random.permutation(labels.size)

        num_labeled = 200

        labeled_data = data[shuffled_indices[:num_labeled], :].T
        labeled_labels = labels[shuffled_indices[:num_labeled]]
        unlabeled_data = data[shuffled_indices[num_labeled:], :].T
        unlabeled_labels = labels[shuffled_indices[num_labeled:]]

        start_time = time.time()
        predicted_labels = lsh_label_prop(labeled_data, labeled_labels,
                                          unlabeled_data)
        cost = time.time() - start_time

        accuracy = np.mean(predicted_labels == unlabeled_labels)

        print("LSH label propagation scored %f accuracy in %f seconds." %
              (accuracy, cost))


if __name__ == '__main__':
    unittest.main()
